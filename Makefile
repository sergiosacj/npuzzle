BIN=npuzzle

all: $(BIN)

$(BIN): $(BIN).cpp
	g++ $^ -o $(BIN)

run:
	python3 ./run_inputs.py $n

clean:
	rm -f *.o $(BIN)
