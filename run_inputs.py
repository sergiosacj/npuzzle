import os
import sys

args = ['3x3', '4x4', '5x5', '6x6']
print(sys.argv)
if len(sys.argv) != 2:
    raise Exception(
        f"\nUso: python3 run_inputs.py 3x3\nArgumentos possíveis: {args}")

if not sys.argv[1] in args:
    raise Exception(f"Argumentos possíveis: {args}")

bin = './npuzzle'
input_path = f"./inputs/{sys.argv[1]}"
output_path = './outputs'
files = sorted(os.listdir(input_path))

for file in files:
    file_input_path = f"{input_path}/{file}"
    output_filename = os.path.splitext(file)[0]
    file_output_path = f"{output_path}/{output_filename}.output"
    cmd = f"{bin} < {file_input_path} > {file_output_path}"
    print(cmd)
    os.popen(cmd).read
